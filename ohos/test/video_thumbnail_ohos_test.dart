import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:video_thumbnail_ohos/video_thumbnail_ohos.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  const MethodChannel('plugins.justsoft.xyz/video_thumbnail').setMockMethodCallHandler((MethodCall methodCall) async {
    if (methodCall.method == 'file' || methodCall.method == 'data') {
      if (methodCall.method == 'file') {
        return '/path/to/thumbnail.jpg';
      } else {
        return Uint8List.fromList([1, 2, 3]);
      }
    }
    return null;
  });

  test('generates thumbnail data', () async {
    final Uint8List? thumbnailData = await VideoThumbnailOhos.thumbnailData(
      video: 'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
      imageFormat: ImageFormat.PNG,
      maxWidth: 800,
      maxHeight: 800,
      quality: 2
    );
    expect(thumbnailData, isNotNull);
  });
}